Steps for getting started
1. Clone the repo
    - give permission to populate.sh using command `chmod +x server.log`
    - run populate.sh using `./populate.sh 20000` this will generate log file consisting of 20000 lines. [number of lines can be changed]
2. Run `node index.js` (Use node version > 8)
3. Hit GET: `localhost:8101` with or without query params {'searchFor'}
    ex: localhost:8101?searchFor=19:17:56 or localhost:8101?searchFor=2019-12-09
