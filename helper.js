const url = require('url');
const { exec } = require('child_process');

exports.getQueryParams = (requestUrl) => {
  return url.parse(requestUrl,true).query;
}

exports.execPromise = (command) => {
  return new Promise((resolve, reject) => {
    exec(command, (err, value) => {
      resolve({ err, value });
    });
  });
}