const fs = require('fs');
const stream = require('stream');
const readline = require('readline');
const http = require('http');
const { execPromise, getQueryParams } = require('./helper');

const port = 8101;
const hostname = 'localhost';

const serve = async (req, res) => {
  try {
    const { searchFor = '' } = getQueryParams(req.url);
    let data = [];
    const fileName = 'server.log'
    if (!searchFor) {
      const { value } = await execPromise(`tail -n 100 ${fileName}`);
      data = value.split(/\r?\n/)
      res.writeHead(200, {'Content-Type': 'application/json'})
      res.write(JSON.stringify(data));
      res.end();
      return;
    }

    const readStream = fs.createReadStream(`${fileName}`, 'utf-8');
    readStream.on('error', (error) => {
      res.writeHead(404, 'Not Found');
      res.end();
    });

    const outStream = new stream();

    const rl = readline.createInterface(readStream, outStream);

    rl.on('line', (line) => {
      if (searchFor && line.includes(searchFor)) {
        data.push(line);
      }
    });

    rl.on('close', () => {
      res.writeHead(200, {'Content-Type': 'application/json'})
      res.write(JSON.stringify(data));
      res.end();
    });
  } catch (e) {
    res.writeHead(400, 'Bad request');
    res.end();
  }
}

const server = http.createServer(serve);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
})