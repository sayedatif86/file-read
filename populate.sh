a=0
arr[0]="Received request from 102.23.55.32 for /example"
arr[1]="Started database query for table users"
arr[2]="Completed database query for table users"
while [ $a -lt $1 ]
do
  current_time=$(date "+%Y-%m-%dT%H:%M:%S")
  rand=$[ $RANDOM % 3 ]
  echo "$current_time ${arr[$rand]}" >> server.log
  a=`expr $a + 1`
done